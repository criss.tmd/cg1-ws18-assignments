#version 150

in vec4 aPosition;
in vec3 aNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

out vec4 vPosition;
out vec3 vNormal;

void main() {
    // Transform to eye-space
    
    vPosition = uModelViewMatrix * aPosition;

    // Transform normals using the correct matrix.
    // It's important to normalize in the end
    mat4 normalMvm = inverse(transpose(uModelViewMatrix));
    vNormal = normalize(vec3( normalMvm * vec4(aNormal,0.0) ));

    gl_Position = uProjectionMatrix * vPosition;
}
