#version 150

in vec4 aPosition;
in vec3 aNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

uniform vec3 uLightPosition;
uniform vec3 uLightColor;
uniform float uLightSpotLightFactor;

uniform mat3 uAmbientMaterial;
uniform mat3 uDiffuseMaterial;
uniform mat3 uSpecularMaterial;
uniform float uSpecularityExponent;

out vec3 vColor;


vec3 ambient()
{
    return uLightColor * uAmbientMaterial;
}


vec3 diffuse(vec4 _position, vec3 _normal)
{
    // position of light in eye space
    vec3 pLight = normalize(uLightPosition-vec3(_position));
    // cos of the angle between point-to-light vector and surface-normal
    float cosAngle = max(dot(pLight, _normal), 0);
    return uDiffuseMaterial * uLightColor * cosAngle;
}


vec3 specular(vec4 _position, vec3 _normal)
{
    // position of light in eye space
    vec3 pLight = normalize(uLightPosition-vec3(_position));
    // bisector of the parallelogram of point-to-light vector and point-to-eye (sum of vectors), normalized
    vec3 bisector = normalize(normalize(-vec3(_position)) + pLight );
    // cos of the angle between bisector vector and normal, non-negative value
    float cosAngle = max(dot(bisector, _normal), 0);
    return uSpecularMaterial * uLightColor * pow(cosAngle, uSpecularityExponent);
}


float spotLight(vec4 _position)
{
    // eye coords in world space
    vec3 originEyeCoords = vec3(inverse(uModelViewMatrix) * vec4(0.0f, 0.0f, 0.0f, 0.0f) );
    // light Direction in world space (from light source)
    vec3 lightDir = normalize(originEyeCoords - uLightPosition);

    vec3 pLight = normalize(vec3(_position) - uLightPosition);

    float cosAngle = abs(dot(pLight, lightDir));

    return  pow(cosAngle, uLightSpotLightFactor);
}


void main() {
    // Transform to eye-space
    vec4 newPosition = uModelViewMatrix * aPosition;

    // Transform normals using the correct matrix.
    // It's important to normalize in the end
    mat4 normalMvm = inverse(transpose(uModelViewMatrix));
    vec3 newNormal = normalize(vec3( normalMvm * vec4(aNormal,0.0) ));

    gl_Position = uProjectionMatrix * newPosition;

    // Define the color of this vertex
    vColor = ambient()
             + spotLight(aPosition) * (diffuse(newPosition, newNormal) + specular(newPosition, newNormal));
}
