#version 150

in vec4 vPosition;
in vec3 vNormal;

uniform mat4 uProjectionMatrix;
uniform mat4 uModelViewMatrix;

uniform vec3 uLightPosition;
uniform vec3 uLightColor;
uniform float uLightSpotLightFactor;

uniform mat3 uAmbientMaterial;
uniform mat3 uDiffuseMaterial;
uniform mat3 uSpecularMaterial;
uniform float uSpecularityExponent;

out vec4 oFragColor; //write the final color into this variable


vec3 ambient()
{
    return uAmbientMaterial * uLightColor;
}


vec3 diffuse(vec4 _position, vec3 _normal)
{
    // position of light in eye space
    vec3 pLight = normalize(uLightPosition-vec3(_position));
    // cos of the angle between point-to-light vector and surface-normal
    float cosAngle = max(dot(pLight, _normal), 0);
    return uDiffuseMaterial * uLightColor * cosAngle;
}


vec3 specular(vec4 _position, vec3 _normal)
{
    // position of light in eye space
    vec3 pLight = normalize(uLightPosition-vec3(_position));
    // bisector of the parallelogram of point-to-light vector and point-to-eye (sum of vectors), normalized
    vec3 bisector = normalize(normalize(-vec3(_position)) + pLight );
    // cos of the angle between bisector vector and normal, non-negative value
    float cosAngle = max(dot(bisector, _normal), 0);
    return uSpecularMaterial * uLightColor * pow(cosAngle, uSpecularityExponent);
}

void main() {
    oFragColor = vec4( ambient()
                        + diffuse(vPosition, vNormal) // diffuse term
                        + specular(vPosition, vNormal), 0.0f); // specular term
}
