/*
 * Basics of Computer Graphics Exercise
 */
 
#include "assignment.h"
using namespace std;

bool convex(const glm::vec2& prev, const glm::vec2& curr, const glm::vec2& next)
{
    // True iff the vertex curr is a convex corner.
    // Assume counter-clockwise winding order.

    glm::vec2 v0 = next-prev;
    glm::vec2 v = curr-prev;
    float prod = v.y*v0.x - v.x*v0.y;
    if (prod<0)
        return true;
    else
        return false;
}

bool inTriangle(const glm::vec2& p, const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
{
    // True iff the point p lies within the triangle a, b, c.
    // Assume counter-clockwise winding order.
    if (convex(a, p, b) || convex(b, p, c) || convex(c, p, a) )
        return false;
    else
        return true;
}

bool triangleEmpty(const int i_a, const int i_b, const int i_c, const std::vector<glm::vec2>& vertices)
{
    // True iff there is no other vertex inside the triangle a, b, c.
    glm::vec2 a = vertices[i_a];
    glm::vec2 b = vertices[i_b];
    glm::vec2 c = vertices[i_c];

    for (int i=0; i<vertices.size(); i++)
        if (i!=i_a && i!=i_b && i!=i_c)
        {
            if (inTriangle(vertices[i], a,b,c) )
                return false;
        }
    
    return true;
}

void triangulate(const std::vector<glm::vec2>& vertices, std::vector<int>& triangles)
{
    // Loop through vertices and clip away ears until only two vertices are left.
    // Input:  "vertices" contains the polygon vertices in counter-clockwise order.
    // Output: "triangles" contains indices into the "vertices" vector. Each triplet
    //         of consecutive indices specifies a triangle in counter-clockwise order.

    int n = vertices.size();
    if (vertices.size() < 3)
        return;

    std::cout << "Starting triangulation of a " << n << "-gon." << std::endl;

    // True iff the vertex has been clipped.
    std::vector<bool> clipped(n, false);

    int a=0, b, c, remaining = n;
    // return;
    std::vector<glm::vec2> remaining_vertices;
    // for (int i=0; i<vertices.size(); i++)
    //     remaining_vertices.push_back(vertices[i]);

    while (remaining >= 3)
    {
        while (clipped[a])
        {
            a = (a+1)%n;
        }
        b = (a+1)%n;
        while (clipped[b])
        {
            b = (b+1)%n;
        }
        c = (b+1)%n;
        while (clipped[c])
        {
            c = (c+1)%n;
        }
        
        int new_a = a, new_b = b, new_c = c;
        remaining_vertices.clear();
        for (int i=0; i<n; i++)
        {
            if (!clipped[i])
                remaining_vertices.push_back(vertices[i]);
            if (i == a)
                new_a = remaining_vertices.size()-1;
            if (i == b)
                new_b = remaining_vertices.size()-1;
            if (i == c)
                new_c = remaining_vertices.size()-1;
        }
        // std::cout << "a= " << a << "b= " << b << "c= " << c << "\n ";
        if (convex(vertices[a], vertices[b], vertices[c]) && triangleEmpty(new_a,new_b,new_c, remaining_vertices))
        {
            clipped[b] = true;
            triangles.push_back(a);
            triangles.push_back(b);
            triangles.push_back(c);
            // std::cout << "good!\n"; 
            remaining--;
            // if (n-remaining == 3)
            // return;
        }
        a = (a+1)%n;
    }
}

void initCustomResources()
{
}

void deleteCustomResources()
{
}
