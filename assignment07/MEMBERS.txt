# Enumerate all team members in this file, one per line, using
# the format <matriculation number> <name>.
# Empty lines and lines starting with a hash symbol '#' are ignored.
# Below is an example entry. Delete it before adding your own entries!

384816 Cristian Raileanu
384209 Ante Pehar
384049 Mengying Xue
345431 Philip Kindermann