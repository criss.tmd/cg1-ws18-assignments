/*
 * Basics of Computer Graphics Exercise
 */
 
#include "assignment.h"
using namespace std;


glm::mat4 getEllipse2DTransform(float x, float y, float scaleX, float scaleY, float rotationAngle)
{
	glm::mat4 scaleTransform = glm::mat4();
	// scaling
	scaleTransform[0][0]=scaleX;
	scaleTransform[1][1]=scaleY;
	scaleTransform[2][2]=1.0f;
	scaleTransform[3][3]=1.0f;

	// rotation
	glm::mat4 transformRotation = glm::mat4();
	transformRotation[0][0] = cos(rotationAngle);
	transformRotation[1][1] = cos(rotationAngle);
	transformRotation[0][1] = sin(rotationAngle);
	transformRotation[1][0] = -sin(rotationAngle);
	transformRotation[2][2] = 1.0f;
	transformRotation[3][3] = 1.0f;

	transformRotation = transformRotation * scaleTransform;
	transformRotation[3][0] = x;
	transformRotation[3][1] = y;

	return transformRotation;
}

glm::mat4 getCarTransform(float time, float speed, float position)
{
	float rotationAngle = fmod(M_PI - time*speed, 2*M_PI);
	float x = position * cos(rotationAngle);
	float y = position * sin(rotationAngle);
	
	return getEllipse2DTransform(x, y, 0.03f, 0.08f, rotationAngle);
}



void drawScene(int scene, float runTime) {
  	
	// 3a -------------------------------------------------------------
	glm::vec3 colorBlue = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 colorGray = glm::vec3(105/255.0f,105/255.0f,105/255.0f);
	glm::vec3 colorBlack = glm::vec3(0.0f, 0.0f, 0.0f);

    drawCircle(colorBlue , getEllipse2DTransform(0, 0, 0.90f, 0.90f, 0) );

	drawCircle(colorGray , getEllipse2DTransform(0, 0, 0.89f, 0.89f, 0) );

	drawCircle( colorBlue, getEllipse2DTransform(0, 0, 0.70f, 0.70f, 0) );

	drawCircle( colorBlack, getEllipse2DTransform(0, 0, 0.69f, 0.69f, 0) );

	// 3b ----------------------------------------------------------------
	glm::vec3 colorStand = glm::vec3(169/255.0f,169/255.0f,169/255.0f);

	drawCircle( colorStand, getEllipse2DTransform(-0.95f, 0, 0.03f, 0.60f, 0) );

	// 3c ----------------------------------------------
	glm::vec3 colorWhite = glm::vec3(1.0, 1.0, 1.0);

	float dx = 0.02f;
	// draw
	for (int i=0; i<=9; i++)
	{	
		drawCircle( colorWhite, getEllipse2DTransform(-0.88f+dx*i, 0, 0.005f, 0.04f, 0) );
	}

	// 3d -----------------------------------------------
	int nrLines = 20;
	float drotationAngle = 2*M_PI / nrLines;
	float rotationAngle = drotationAngle/2;
	float distance=0.79f;
	
	for (int i=0; i<=nrLines; i++)
	{
		float x = distance * cos(rotationAngle);
		float y = distance * sin(rotationAngle);
		
		drawCircle( colorWhite, getEllipse2DTransform(x, y, 0.01f, 0.04f, rotationAngle) );
		rotationAngle += drotationAngle;
	}
	
	// 3e ---------------------------------------------
	glm::vec3 colorCar1 = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 colorCar2 = glm::vec3(1.0f, 1.0f, 0.0f);

	
	// draw car 1
	drawCircle( colorCar1, getCarTransform(runTime, 2.0f, 0.84f) );
	drawCircle( colorCar2, getCarTransform(runTime, 1.0f, 0.75f) );
}

void initCustomResources()
{
}

void deleteCustomResources()
{
}

